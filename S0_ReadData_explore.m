%% S0_ReadData_explore.m
%--------------------------------------------------------------------------
% created by Ines Adriaens (@adria036)
% created November 3rd, 2021
%--------------------------------------------------------------------------
% STEP 0: set filepaths and constants
% STEP 1: load data from input folder
% STEP 2: summary stats
%--------------------------------------------------------------------------


clear variables
close all
clc

% set datetime formats
datetime.setDefaultFormats('default','dd/MM/yyyy HH:mm:ss')
datetime.setDefaultFormats('defaultdate','dd/MM/yyyy')


%% STEP 0: filepaths and constants

% data folder
ddir = ['C:\Users\adria036\OneDrive - Wageningen University & Research\' ...
        'iAdriaens_doc\Projects\cKamphuis\nlas\data\'];
    
% date of data stored
ddate = '20211103\';

% list files in folder ddir\ddate
files = ls([ddir ddate '*.csv']);

%------------------------ file name structure------------------------------
% 'Tracklab Export yyyyMondd HH_mm_ss 
%           Session[yyyyMondd HH_mm_ss - yyyyMondd HH_mm_ss]
%           TAGID(14chars).csv
%------------------------ file name structure------------------------------

% store information in table
filedata = table(files(:,end-17:end-4),'VariableNames',{'TagID'});
filedata.dateBU = datetime(files(:,17:33),'InputFormat','yyyyMMMdd HH_mm_ss');
filedata.datestart = datetime(files(:,44:61),'InputFormat','yyyyMMMdd HH_mm_ss');
filedata.dateend = datetime(files(:,65:82),'InputFormat','yyyyMMMdd HH_mm_ss');
filedata.cowID = [repmat('cow_',[height(filedata),1]), num2str((1:height(filedata))')];
filedata.fn = files;


%% STEP 1: load data

% read csv files
clear alldata
for i = 1:height(filedata)
    % fieldname
    field_ = filedata.cowID(i,:);
    
    % read options
    opts = detectImportOptions([ddir ddate filedata.fn(i,:)]);
%     opts = setvaropts(opts,'TimeStamp','DatetimeLocale','nl_NL'); 
    opts = setvaropts(opts,'TimeStamp','InputFormat','MM/dd/yyyy HH:mm:ss.SSS'); 
    opts = setvartype(opts,'TagId','char'); 
    
%     getvaropts(opts,'TimeStamp')
    
    % read data
    data.(field_) = readtable([ddir ddate filedata.fn(i,:)],opts);
    
    % combine in single array
    if ~exist('alldata','var')
        alldata = data.(field_);
    else
        alldata = [alldata;data.(field_)];
    end
end

% clear workspace
clear i opts files ddate ddir ans field_

% combine data in single array

%% STEP 2: exporatory analysis

% add time difference
alldata.timediff(1) = NaN;
alldata.timediff(2:end) = diff(datenum(alldata.TimeStamp));
alldata.timediff(alldata.timediff < 0) = NaN;
alld.summary = groupsummary(alldata,'TagId',{'mean','std','median','min','max'});



%% frequency of cow location
close all
% fields
fields_ = fieldnames(data);

h = [];

% round x and y data on 50 cm (=1/2m)
alldata.roundX(:,1) = round(alldata.X*2)/2;
alldata.roundY(:,1) = round(alldata.Y*2)/2;
    
    
% cows in data
cows = unique(alldata.TagId);

for i = 1:length(cows)
    
    % fieldname
    field_ = sprintf('cow_%d',i);
    
    % figure
    h.(field_) = figure('Units','centimeters','OuterPosition',[25 12 26 14]); box on; 
    
    
    % alldata of cow
    ind = find(contains(alldata.TagId,cows{i}));

    hi = heatmap(alldata(ind,:),'roundX','roundY',...
        'ColorMethod','count',...
        'GridVisible',0,...
        'FontSize',8,...
        'ColorbarVisible','off', ...
        'Title', field_ ) ;
    ax = axes;hold on;box on
    plot(ax,[-0.5 32],[4 4],'k--','LineWidth',1)
    plot(ax,[10.5 10.5], [-0.5 19],'k--','LineWidth',1)
    plot(ax,[21 21], [-0.5 19],'k--','LineWidth',1)
    plot(ax,0,0,'s','MarkerSize',7,'MarkerFaceColor','m','Color','m')
    plot(ax,0,18.5,'s','MarkerSize',7,'MarkerFaceColor','m','Color','m')
    plot(ax,10,0,'s','MarkerSize',7,'MarkerFaceColor','m','Color','m')
    plot(ax,10,18.5,'s','MarkerSize',7,'MarkerFaceColor','m','Color','m')
    plot(ax,21.5,0,'s','MarkerSize',7,'MarkerFaceColor','r','Color','r')
    plot(ax,21.5,18.5,'s','MarkerSize',7,'MarkerFaceColor','r','Color','r')
    plot(ax,31.5,0,'s','MarkerSize',7,'MarkerFaceColor','r','Color','r')
    plot(ax,31.5,18.5,'s','MarkerSize',7,'MarkerFaceColor','r','Color','r')

    ax.XTick = '';ax.YTick = '';
    ax.Color = 'none'; xlim([-0.5 32]);ylim([-0.5 19])
    

    
%     ax = gca;
%     ax.XDisplayLabels = nan(size(ax.XDisplayData));
%     ax.YDisplayLabels = nan(size(ax.YDisplayData));

%     if i == size(fields_,1)
%         saveas(h.(plt.cowfighandle),[paths.resdir 'FIG_heatmapPosition_' num2str(cows(j))])
%     end
end


