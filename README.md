# NLAS - exploratory analysis and data quality check

>Created by @adria036 (Ines Adriaens)  
>Created November 3rd, 2021  

This repo will contain the scripts for data exploration of NLAS project 2021 since new installation of the system October and November 2021 -- for now, primarily coded in Matlab.

## Data 

Exported from tracklab system at DC.  
Format : ".csv" files

## Scripts

1. Detect files and load data
2. Summaries of basic statistics
3. Figures
